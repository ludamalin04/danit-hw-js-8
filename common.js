'use script'
// 1
const paragraphEl = document.getElementsByTagName('p');
for (const kids of paragraphEl){
    kids.style.background = '#ff0000';
}

// 2
const optionsEl = document.getElementById('optionsList');
console.log(optionsEl);
const parentOptionsEl = optionsEl.parentElement;
console.log(parentOptionsEl);
const kidsOptionsEl = optionsEl.childNodes;
console.log(kidsOptionsEl);

// 3
const testParagraphEl = document.querySelector('#testParagraph');
testParagraphEl.innerHTML = `This is a paragraph`;

// 4
const mainHeaderEl = document.querySelector('.main-header');
const kidsMainHeaderEl = mainHeaderEl.children;
console.log(kidsMainHeaderEl);
for (const kids of kidsMainHeaderEl){
    kids.classList.add('nav-item');
}

// 5
const sectionTitleEl = document.querySelectorAll('.section-title');
console.log(sectionTitleEl);
for (const kids of sectionTitleEl ){
    kids.classList.remove('section-title');
}

